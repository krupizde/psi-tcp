/**
 * 
 */
package net.krupizde.main;

import net.krupizde.main.network.MyServerSocket;

/**
 * @author MrScrufik
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		boolean run = true;
		int portNumber = -1;
		try {
			if (args.length == 1)
				portNumber = Integer.parseInt(args[0]);
			else
				run = false;
		} catch (NumberFormatException e) {
			run = false;
		}
		if (run) {
			MyServerSocket socket = new MyServerSocket(portNumber);
			socket.run();
		} else {
			System.err.println("Usage: java -jar app.jar <port number>");
		}
	}

}
