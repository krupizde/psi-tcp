/**
 * 
 */
package net.krupizde.main.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import net.krupizde.main.logic.RobotCommnader;

/**
 * @author MrScrufik
 *
 */
public class MyClientSocket extends Thread {

	private Socket soc;

	public MyClientSocket(Socket soc) {
		this.soc = soc;
	}

	@Override
	public void run() {
		PrintWriter clientSocketWriter = null;
		BufferedReader clientSocketReader = null;
		
		try {
			clientSocketWriter = new PrintWriter(soc.getOutputStream(), true);
			clientSocketReader = new BufferedReader(new InputStreamReader(soc.getInputStream()));
			RobotCommnader commander = new RobotCommnader(clientSocketReader, clientSocketWriter);
			if(!commander.autentizace()) {
				commander.loginFailed();
				soc.close();
				return;
			}
			while (true) {
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	

}
