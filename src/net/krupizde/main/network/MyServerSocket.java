/**
 * 
 */
package net.krupizde.main.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * @author MrScrufik
 *
 */
public class MyServerSocket extends Thread {

	private int port = -1;

	public MyServerSocket(int port) {
		this.port = port;
	}

	@Override
	public void run() {
		Socket clientSocket = null;
		MyClientSocket soc = null;
		ArrayList<MyClientSocket> clientSockets = new ArrayList<>();
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(port);
			while (true) {
				clientSocket = serverSocket.accept();
				soc = new MyClientSocket(clientSocket);
				soc.start();
				clientSockets.add(soc);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (serverSocket != null)
					serverSocket.close();
			} catch (IOException e) {
			}
		}

	}

}
