package net.krupizde.main.logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class RobotCommnader {
	private PrintWriter clientSocketWriter = null;
	private BufferedReader clientSocketReader = null;
	private static final int SERVER_KEY = 54621;
	private static final int CLIENT_KEY = 45328;
	private static final String SEQ = "\\a\\b";

	public RobotCommnader(BufferedReader clientSocketReader, PrintWriter clientSocketWriter) {
		this.clientSocketReader = clientSocketReader;
		this.clientSocketWriter = clientSocketWriter;
	}

	public boolean autentizace() {
		try {
			String name = read(12);
			if(name.isEmpty())return false;
			name = name.substring(0, name.indexOf(SEQ));
			int hash = 0, clientHash = 0;
			for (int i = 0; i < name.length(); i++) {
				hash += (int) name.charAt(i);
			}
			clientHash = hash;
			hash = (((hash * 1000) % 65536) + SERVER_KEY) % 65536;
			clientSocketWriter.write(hash + SEQ);
			clientHash = (((clientHash * 1000) % 65536) + CLIENT_KEY) % 65536;
			String confirm = read(7);
			if(confirm.isEmpty())return false;
			confirm = confirm.substring(0, name.indexOf(SEQ));
			if (Integer.parseInt(confirm) != clientHash)
				return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	private String read(int maxLen) throws IOException {
		String out = "";
		while (true) {
			out += clientSocketReader.readLine();
			if (out.contains(SEQ) && out.length() <= maxLen && out.length() >= 3)
				break;
			else if (out.contains(SEQ) && out.length() > maxLen)
				return "";
			else if (out.length() > (maxLen - 2))
				return "";
		}
		return out;
	}

	public void loginFailed() {
		clientSocketWriter.write("300 LOGIN FAILED\\a\\b");
	}
}
